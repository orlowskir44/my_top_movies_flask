from flask import Flask, render_template, redirect, url_for, request
from flask_bootstrap import Bootstrap5
from flask_sqlalchemy import SQLAlchemy
from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, FloatField
from wtforms.validators import DataRequired, Length
import requests

## REQUESTS:
headers = {
    "accept": "application/json",
    "Authorization": "Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJkYTFjMzFlM2MxMGFmMTNmZTE4MTA0ZDBiM2FjZWYzZCIsInN1YiI6IjY0ZTRhODQ3YzYxM2NlMDEwYjhiMDBiNSIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.DI_nZ-D5MQz99yNiiN2PDnhdvFZeCWVTwKNmqqEf27k"
}

## INIT:
app = Flask(__name__)
app.config['SECRET_KEY'] = '8BYkEfBA6O6donzWlSihBXox7C0sKR6b'
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///new-books-collection.db"

db = SQLAlchemy()
db.init_app(app)

Bootstrap5(app)


## DB:
class Movie(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(250), unique=True, nullable=False)
    year = db.Column(db.Integer, nullable=False)
    description = db.Column(db.String(250), nullable=False)
    rating = db.Column(db.Float, nullable=True)
    ranking = db.Column(db.Integer, nullable=True)
    review = db.Column(db.String(250), nullable=True)
    img_url = db.Column(db.Text, nullable=False)


with app.app_context():
    db.create_all()


## FLASK FORMS:
## EDIT MOVIE:
class EditForm(FlaskForm):
    rating = FloatField('Your rating (0-10)', validators=[DataRequired()])
    review = StringField('Your review', validators=[DataRequired()])
    submit = SubmitField('Done')


## CREATE MOVIE:
class CreateForm(FlaskForm):
    title = StringField('Movie Title', validators=[DataRequired()])
    submit = SubmitField('Add movie')


##


@app.route("/")
def home():
    movies = db.session.execute(db.select(Movie).order_by(Movie.rating.desc()))

    get_movies = movies.scalars().all()

    r = 1
    for i in get_movies:
        i.ranking = r
        r += 1

    db.session.commit()
    return render_template("index.html", movies=get_movies)


@app.route('/edit', methods=['GET', 'POST'])
def edit():
    edit = EditForm()

    id = request.args.get('id')
    mov = db.get_or_404(Movie, id)

    if edit.validate_on_submit():
        mov.rating = request.form['rating']
        mov.review = request.form['review']
        db.session.commit()
        return redirect(url_for('home'))

    return render_template('edit.html', edit=edit)


@app.route('/delete')
def delete():
    id = request.args.get('id')
    mov = db.get_or_404(Movie, id)

    db.session.delete(mov)
    db.session.commit()
    return redirect(url_for('home'))


@app.route('/add', methods=['GET', 'POST'])
def add():
    create = CreateForm()

    if create.validate_on_submit():
        url = f"https://api.themoviedb.org/3/search/movie?query={request.form['title']}&page=1"
        response = requests.get(url, headers=headers).json()
        # movies = [x['original_title'] for x in response['results']]
        data = response['results']
        return render_template('select.html', movies=data)

    return render_template('add.html', create=create)


@app.route('/add/data')
def get_md():
    id = request.args.get('id')

    url = f"https://api.themoviedb.org/3/movie/{id}"
    response = requests.get(url, headers=headers)
    data = response.json()

    new_movie = Movie(
        title=data['original_title'],
        year=data['release_date'],
        description=data['overview'],
        rating='12',
        img_url=f"https://image.tmdb.org/t/p/w500/" + data['poster_path']
    )

    db.session.add(new_movie)
    db.session.commit()

    return redirect(url_for('edit', id=new_movie.id))


if __name__ == '__main__':
    app.run(debug=True, port=8000)
